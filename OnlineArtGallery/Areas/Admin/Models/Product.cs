﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OnlineAreGallery.Areas.Admin.Models
{
    [Table("Product")]
    public class Product
    {
        [Key]
        [Required]
        public string ProductId { get; set; }
        [Required]
        public string UserId { get; set; }
        [Required]
        public string ProductName { get; set; }
        [Required]
        public string Artist { get; set; }
        [Required]
        public string BidPrice { get; set; }
        [Required]
        public string FixedPrice { get; set; }
        [Required]
        public string Duration { get; set; }
        [Required]
        public string Origin { get; set; }
        [Required]
        public string Condition { get; set; }
        [Required]
        public string ShortDescription { get; set; }
        [Required]
        public string LongDescription { get; set; }
        [Required]
        public string Status { get; set; }
        [Required]
        public string CreatAt { get; set; }
        [Required]
        public string CategoryId { get; set; }
        [Required]
        public string TypeId { get; set; }
        [Required]
        public string PhotoName { get; set; }
    }
}
