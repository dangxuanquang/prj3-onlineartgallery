﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

// Author: Quang Dang - New Art Exhibiton

$(document).ready(function () {
    var readURL = function (input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.profile-pic').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".file-upload").on('change', function () {
        readURL(this);
    });

    $(".upload-button").on('click', function () {
        $(".file-upload").click();
    });
});

// End Display image
// ------------------

// Display image url for .custom-file-input

$(".custom-file-input").on("change", function () {
    var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});

// End Display image
// ------------------

// Function show accordion Helps page

var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function () {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.maxHeight) {
            panel.style.maxHeight = null;
        } else {
            panel.style.maxHeight = panel.scrollHeight + "px";
        }
    });
}
// End function show accordion
// ------------------


// Function show count down multiple place on same page

timedown("May 20, 2021 17:00:25", 'demo');
timedown("January 10, 2021 17:00:25", 'demo2');
timedown("January 20, 2021 17:00:25", 'demo3');
timedown("February 20, 2021 17:00:25", 'demo4');
timedown("March 10, 2021 17:00:25", 'demo5');
timedown("April 20, 2021 17:00:25", 'demo6');
timedown("May 20, 2021 17:00:25", 'demo7');
timedown("September 10, 2021 17:00:25", 'demo8');
timedown("August 20, 2021 17:00:25", 'demo9');
timedown("December 20, 2021 17:00:25", 'demo10');
timedown("October 10, 2021 17:00:25", 'demo11');
timedown("January 20, 2021 17:00:25", 'demo12');
function timedown(ti, id) {
    // Set the date we're counting down to
    var countDownDate = new Date(ti).getTime();

    // Update the count down every 1 second
    var x = setInterval(function () {

        // Get todays date and time
        var now = new Date().getTime();

        // Find the distance between now an the count down date
        var distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Output the result in an element with id="demo"
        document.getElementById(id).innerHTML = (days != 0 ? days + "d " : '') + (hours != 0 ? hours + "h " : '')
            + (minutes != 0 ? minutes + "m " : '') + seconds + "s ";

        // If the count down is over, write some text 
        if (distance < 0) {
            clearInterval(x);
            document.getElementById("#demo").innerHTML = "EXPIRED";
        }
    }, 1000);
}

